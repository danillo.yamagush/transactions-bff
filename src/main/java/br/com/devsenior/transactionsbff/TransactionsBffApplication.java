package br.com.devsenior.transactionsbff;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
@EnableRedisRepositories(basePackages = {"br.com.devsenior.transactionsbff.redis"})
public class TransactionsBffApplication {

    public static void main(String[] args) {
        SpringApplication.run(TransactionsBffApplication.class, args);
    }

}
