package br.com.devsenior.transactionsbff.entity.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;

import java.io.Serializable;

public class BeneficiarioDto implements Serializable {

    private static final long serialVersionUID = 2205392612060907566L;

    @Schema(description = "CPF do Beneficiario")
    @NotNull(message = "Informar o CPF")
    private Long CPF;

    @NotNull(message = "Informa o código do banco de destino")
    @Schema(description = "Código do banco destino")
    private Long codigoBanco;

    @NotNull(message = "Informar a agência de destino")
    @Schema(description = "Agência de destino")
    private String agencia;

    @NotNull(message = "Informar a conte de destino")
    @Schema(description = "Conta de destino")
    private String conta;

    @NotNull(message = "Informar o nome do favorecido")
    @Schema(description = "Nome do Favorecido")
    private String nomeFavorecido;
}
