package br.com.devsenior.transactionsbff.entity;

import br.com.devsenior.transactionsbff.entity.dto.BeneficiarioDto;
import br.com.devsenior.transactionsbff.entity.enums.SituacaoEnum;
import br.com.devsenior.transactionsbff.entity.enums.TipoTransacaoEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Conta implements Serializable {

    private static final long serialVersionUID = -4646752503789154157L;

    @Schema(description = "Código da Agência")
    @NotNull(message = "Informar o código da Agência")
    private Long codigoAgencia;

    @Schema(description = "Código da Conta")
    @NotNull(message = "Informar o código da Conta.")
    private Long codigoCOnta;

    @Schema(description = "Beneficiário da transação")
    @Valid
    private BeneficiarioDto beneficiarioDto;

    @NotNull(message = "Informar o tipo da transação")
    @Schema(description = "Tipo de transação")
    private TipoTransacaoEnum tipoTransacao;

    @Schema(description = "Situação da transação")
    private SituacaoEnum situacaoEnum;
}
