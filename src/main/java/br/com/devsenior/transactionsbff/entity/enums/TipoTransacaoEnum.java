package br.com.devsenior.transactionsbff.entity.enums;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema
public enum TipoTransacaoEnum {
    PAGAMENTO_TRIBUTOS,
    PAGAMENTO_TITULOS,
    TED,
    DOC;
}
