package br.com.devsenior.transactionsbff.redis;

import br.com.devsenior.transactionsbff.entity.dto.TransactionDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRedisRepository extends CrudRepository<TransactionDto, String> {
}
