package br.com.devsenior.transactionsbff.domain;

import br.com.devsenior.transactionsbff.entity.dto.RequestTransactionDto;
import br.com.devsenior.transactionsbff.entity.dto.TransactionDto;
import br.com.devsenior.transactionsbff.redis.TransactionRedisRepository;
import lombok.AllArgsConstructor;
import org.springframework.cglib.core.Local;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Optional;

@AllArgsConstructor
@Service
public class TransactionService {

    private TransactionRedisRepository transactionRedisRepository;
    private RedisTemplate<String, String> redisTemplate;

    @Transactional
    public Optional<TransactionDto> save(final RequestTransactionDto requestTransactionDto) {
        requestTransactionDto.setData(LocalDateTime.now());
        return Optional.of(transactionRedisRepository.save(requestTransactionDto));
    }

    public Optional<TransactionDto> findById(final String id) {
        return transactionRedisRepository.findById(id);
    }
}
